-- A telescope for whatever you want to find. Like a grep
return {
        'nvim-telescope/telescope.nvim',
        dependencies = { 'nvim-lua/plenary.nvim' },
    }
