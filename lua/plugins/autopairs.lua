-- Pairs with vim motions
return {
    "windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
}
