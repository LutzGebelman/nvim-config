-- Dynamic line on the bottom of the screen
return {
        'nvim-lualine/lualine.nvim',
        dependencies = 'nvim-tree/nvim-web-devicons'
    }
