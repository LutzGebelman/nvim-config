local vim = vim -- A really stupind workaround to stop lsp from screaming at me everywhere about vim not being defined
local keymap = vim.keymap
local wk = require("which-key")
keymap.terminal = {
    bid = nil
}

vim.g.mapleader = ' '

keymap.set('n', '<leader>u', ':UndotreeToggle<CR>', { desc = "Open undo tree" })

-- open the directory tree
-- File manager
keymap.set('n', '<leader><leader>', ':Oil<CR>', { desc = "File manager" })

keymap.set('n', '<esc>', ':sil nohl<CR>', { desc = "Dismiss highlight" }) -- Dismiss highlight 
keymap.set('t', '<esc>', '<C-\\><C-n>', { desc = "Exit terminal input" }) -- Exit terminal input

-- Buffers
keymap.set({'n', 'v'}, '<leader>p', '"+p', { desc = "Paste from the system clipboard" }) -- Paste from a system clipboard (I know about ctrl-shift-v, I just wanted more comfortable way to do it)
keymap.set({'n', 'v'}, '<leader>y', '"+y', { desc = "Yank to the system clipboard" }) -- Copy to systemwide clipboard
-- keymap.set({'n', 'v'}, '', '"+yy') -- Shortcut to copy line to the systemwide clipboard
keymap.set({'n', 'v'}, '<leader>d', '"_d', { desc = "Delete without copy" }) -- Delete without copy
-- keymap.set({'n', 'v'}, 'DD', '"_dd') -- Shortcut to delete line without copy

-- Telescope bindings
local builtin = require('telescope.builtin')

keymap.set('n', '<leader>f', '', { desc = "Telescope" }) -- Find files
keymap.set('n', '<leader>ff', builtin.find_files, { desc = "Find files" }) -- Find files
keymap.set('n', '<leader>fg', builtin.live_grep, { desc = "Live grep" }) -- Live grep
keymap.set('n', '<leader>fh', builtin.help_tags, { desc = "Help tags" }) -- Help tags

keymap.set('n', '<leader>b', '', { desc = "Buffers" })
keymap.set('n', '<leader>bn', ':bnext<CR>', { desc = "Next buffer" })
keymap.set('n', '<leader>bp', ':bprevious<CR>', { desc = "Previous buffer" })
keymap.set('n', '<leader>bN', ':badd<CR>', { desc = "Add a new buffer" })
keymap.set('n', '<leader>bd', ':bd<CR>', { desc = "Delete current buffer" })
keymap.set('n', '<leader>bb', builtin.buffers, { desc = "Buffers" }) -- Buffers


-- Open new window with a terminal

keymap.set('n', '<leader>r', function() require("toggle-bool").toggle_bool() end)
wk.add ( {{ "<leader>r", desc = "Flip bool" }} )
