vim.o.linebreak = true
vim.o.termguicolors = true
vim.o.number = true
vim.o.rnu = true
vim.o.signcolumn = 'yes'
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.expandtab = true
vim.o.termbidi = true
vim.o.cursorline = true
vim.o.colorcolumn = "80"
vim.o.wrap = false


require('plugins')
require('keybindings')
require('theme')
require('lualine-conf')
require('nvim-lspconfig')
